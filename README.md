# Tree Server Test

This is the repo for a coding test. The idea behind this was to create, without using any external libraries, a small server that would only accept post requests to /. This post request should contain a json of the format `{"favoriteTree": "<tree>"}` (but could have more items inside). If it was not stated (or stated "") then it shows a different message.
